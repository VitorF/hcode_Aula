

<?php 
class Usuario{

function __construct($usuarioId = "" , $usuario = "" , $senha = "" , $dataCadastro = ""){
    $this->usuario = $usuario;
    $this->senha = $senha;
    $this->dataCadastro = $dataCadastro;
    $this->usuarioId = $usuarioId;
	}

private $usuarioId;
private $usuario;
private $senha;
private $dataCadastro;



public function getUsuaroId(){
return $this->usuarioId;
}

public function setUsuarioId($usuarioId){
$this->usuarioId = $usuarioId ;
}


public function getUsuario(){
return $this->usuario;
}

public function setUsuario($usuario){
$this->usuario = $usuario;
}

public function getSenha(){
return $this->senha;
}

public function setSenha($senha){
$this->senha = $senha ;
}

public function getDataCadastro(){
return $this->dataCadastro;
}

public function setDataCadastro($dataCadastro){
$this->dataCadastro = $dataCadastro ;
}




public function loadUser($id){

$sql =  new Sql();
$result  = $sql->select("select * from hcode.usuario where usuario_id  = :id",array(':id' => $id ));

if(isset($result[0])){
$this->usuarioId = $result[0] ["usuario_id"];
$this->usuario = $result[0] ["usuario_login"];
$this->senha = $result[0] ["senha"];
$this->dataCadastro = new DateTime($result[0] ["data_cadastro"]);
}


}

function __toString(){
	return  json_encode(array('usuarioId' =>$this->getUsuaroId() ,'usuario' => $this->getUsuario(),'senha'=>$this->getSenha(),'data_cadastro' =>$this->getDataCadastro()->format("d / m / Y")));
}

}


 ?>