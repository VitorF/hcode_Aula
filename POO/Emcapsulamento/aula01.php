<?php 

class Pessoa{

public  $nome = "Vitor Felipe";
protected  $email = "feliperp321@gmail.com" ;
private  $senha = 123456; 



public function verDados(){

echo $this->nome."</br>";
echo $this->email."</br>";
echo $this->senha."</br>";
}


}

$obj  =  new Pessoa();

//atributo public poder ser acessado por qualquer um mesma class , classes filhas e objetos
//echo $obj->nome;

//Atributo protegido só pode ser acessado na mesma classe e classes filhas 
//echo $obj->email;

//Atributo privado só pode ser acessada na mesma classe

//echo $obj->senha;

//Metodo public que acessa todas os valores dos atributos

$obj->verDados();

 ?>